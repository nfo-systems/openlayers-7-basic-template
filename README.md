# OpenLayers 7 Basic Template

No Node.js based setup.

1. Download OpenLayers v7 library package from https://openlayers.org/download/
2. Unzip the package.
3. Create a directory to store OL7 files inside this project, by example *ol7*.
4. Copy OL7 files from unzipped package to the new directory inside this project.


### Make sure to update importing lines in *app.js*

Change *"ol7"* with the directory name used to store OL7 files.

```js
import OSM from './ol7/source/OSM.js';
import Map from './ol7/Map.js';
import TileLayer from './ol7/layer/Tile.js';
import View from './ol7/View.js';
...
```

---

### Alternative with Skypack

*"Load optimized npm packages with no install and no build tools."*

- [OL + Skypack](https://github.com/openlayers/ol-skypack)
- [About Skypack](https://www.skypack.dev/)