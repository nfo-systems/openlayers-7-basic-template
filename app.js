import OSM from './ol7/source/OSM.js';
import Map from './ol7/Map.js';
import TileLayer from './ol7/layer/Tile.js';
import View from './ol7/View.js';

const map = new Map(
    {
        target: 'map',
        layers: [
            new TileLayer(
                {
                    source: new OSM(),
                }
            ),
        ],
        view: new View(
            {
                center: [0, 0],
                zoom: 2,
            }
        ),
    }
);